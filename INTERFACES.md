## Interfaces

Los distintos modelos de Reespirator 2020 utilizan diferentes tipos de interfaces de usuario según la disponibilidad en el lugar de fabricación.

El modelo con PLC utiliza una pantalla táctil HMI lo que permite gran versatilidad y facilidad de uso; pero en los sitios dónde esto no es posible se puede utilizar para el modelo de Arduino una simple pantalla LCD y un encoder u otras configuraciones diferentes como por ejemplo un PC conectado por puerto serie al Arduino o una pantalla táctil conectada a una Raspberry Pi o una Beagle Bone. En todos estos casos un software programado en Python permite representar los datos enviados desde el bloque de control en la pantalla correspondiente y enviar desde el dispositivo apropiado las órdenes de cambio de parámetros al bloque de control.
